function OrderForm($scope) {
    $scope.order = {
        items: [{
            qty: 1,
            description: '',
            cost: 0
        }]
    };

    $scope.addItem = function() {
        $scope.order.items.push({
            qty: 1,
            description: '',
            cost: 0
        });
    },

        $scope.removeItem = function(index) {
            $scope.order.items.splice(index, 1);
        },

        $scope.total = function() {
            var total = 0;
            angular.forEach($scope.order.items, function(item) {
                total += item.qty * item.cost;
            })

            return total;
        }
}